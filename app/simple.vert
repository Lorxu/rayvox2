#version 450 core
in vec2 vertex_pos;
uniform vec3 camera_pos;
uniform vec3 camera_dir;
uniform vec3 camera_up;
uniform float rat;
out vec3 dir;

void main() {
    dir = normalize(camera_dir - (rat * vertex_pos.x * cross(camera_dir, camera_up)) + (vertex_pos.y * camera_up));
    gl_Position = vec4(vertex_pos.x, vertex_pos.y, 0, 1);
}