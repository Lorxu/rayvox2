#version 450 core
in vec3 dir;
uniform vec3 camera_pos;
layout(location = 0) out vec4 fragColor;

vec3 sunDir = vec3(0.2,0.5,1);
vec3 sunCol = vec3(6,5.5,5);

struct Result {
    float t;
    uint mat;
    vec3 normal;
};

// TODO SVOs

float pi = 3.1415926535;
float pii = 1/pi;
float seed = 3;
float mapSize = 16;

uint grid(vec3 pos) {
    float h = sin(0.02 * (seed * pos.x + seed * pos.y)) * (mapSize / 3);
    if (pos.z > h)
        return 0;
    return 1;
}

Result trace(vec3 ro, vec3 rd) {
    vec3 p       = floor(ro); // Cell position
    vec3 m       = vec3(0);   
    vec3 ri      = 1/rd;      // Precompute division
    vec3 rayStep = sign(rd);  // Whether to advance the ray + or - on each axis
    vec3 d       = ((p - ro) + 0.5 + rayStep * 0.5) * ri; // Distance to the next voxel
    int r        = 0;         // Iterations

    while (r <= 128 && grid(p) == 0) {
        m = step(d, d.yzx) * step(d, d.zxy); // Axis on which d is smallest (branchless)
        p += m * rayStep;
        d += m * rayStep * ri;
        r += 1;
    }

    vec3 mini = (p - ro + 0.5 - 0.5 * rayStep) * ri; // Intersect the voxel
    float t    = max(mini.x, max(mini.y, mini.z));

    Result ret;
    ret.t = t;
    ret.mat = grid(p);
    ret.normal = -m * rayStep;
    return ret;
}

vec3 mc(uint mat) {
    switch (mat) {
        case 1:
            return pii * vec3(0.3,1.,0.2);// break;
        case 2:
            return pii * vec3(0.6,0.6,0.6);// break;
    };
    return vec3(0);
}

float compress(vec3 normal, uint mat) { // Normal+mat -> float for storage in fragColor.w
    float result = float(mat);
    vec3 sn = sign(normal);
    if(sn==vec3(1,0,0))
        result += 0.1;
    else if(sn==vec3(-1,0,0))
        result += 0.2;
    else if(sn==vec3(0,1,0))
        result += 0.3;
    else if(sn==vec3(0,-1,0))
        result += 0.4;
    else if(sn==vec3(0,0,1))
        result += 0.5;
    else
        result += 0.6;
    return result;
}

void main() {
    Result r = trace(camera_pos, dir);
    vec3 hitPos = dir * r.t + camera_pos;
    //Result shadow = trace(hitPos + r.normal * 0.01, sunDir);
    //vec3 col = shadow.mat == 0 ? sunCol * mc(r.mat) * vec3(dot(r.normal, sunDir)) : vec3(0);
    fragColor = vec4(hitPos, compress(r.normal, r.mat));
}