// Pretty much just outputs the photon map
#version 450 core

layout(binding = 0, r32ui) uniform uimage2D photon_map;
out vec4 fragColor;

void main(){
    fragColor = vec4(
        imageLoad(photon_map, 
            ivec2(
                (gl_FragCoord.xy)
            )
        ).x,
        0,
        0,
        1
    );
}