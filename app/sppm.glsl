// Compute shader to do photon tracing
#version 450 core

layout(local_size_x = 32, local_size_y = 32) in;
//in uvec3 gl_GlobalInvocationID; // The seed for any randomness that may occur. Note that it's not dependant on time, which avoids any flickering.
                                //      Of course, multiple iterations should have different seeds, so should add that in eventually.

layout(binding = 0, r32ui) uniform uimage2D photon_map; // R32UI
layout(binding = 0) uniform sampler2D hit_map; // RGBA - rgb is hitpos, a is compress(normal, mat)
uniform vec3 camera_pos;

void main() {
    imageStore(photon_map, 
        ivec2(gl_GlobalInvocationID.xy), 
        uvec4(
            texelFetch(hit_map, 
                ivec2(gl_GlobalInvocationID.xy),
                0
            ).a
        )
    );
}