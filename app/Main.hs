-- Rewrite to use OpenGL and GLFW-b
{-# LANGUAGE TemplateHaskell #-}

module Main where
import Prelude hiding ((.)) -- Using . from Categories

import qualified Graphics.UI.GLFW as GLFW
import qualified Graphics.GL as GLR -- OpenGLRaw
import Data.StateVar (($=), get)
import qualified Graphics.Rendering.OpenGL as GL
import Foreign (nullPtr)
import Graphics.GLUtil
import Control.Monad (unless, when)
import Data.Maybe (fromMaybe)
--import qualified Data.Text as Text
--import qualified Data.Text.Encoding as Text
import FRP.Netwire hiding (when, unless)
import Control.Wire hiding (when, unless)
import Linear
import GHC.Float -- for double2Float
import Control.Lens (view)
import Data.FileEmbed

data Proxy a = Proxy
{-startX :: Num a => a
startX = 800
startY :: Num a => a
startY = 600-}

main = do
    e <- GLFW.init
    when e $ do
        GLFW.windowHint $ GLFW.WindowHint'ContextVersionMajor 4
        GLFW.windowHint $ GLFW.WindowHint'ContextVersionMinor 5
        --GLFW.windowHint $ GLFW.WindowHint'Resizable True
        GLFW.windowHint $ GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core

        monitor <- GLFW.getPrimaryMonitor
        videoMode <- case monitor of
            Just m  -> GLFW.getVideoMode m
            Nothing -> return $ Just $ GLFW.VideoMode 800 600 8 8 8 60
        let vm = fromMaybe (GLFW.VideoMode 800 600 8 8 8 60) videoMode
            (GLFW.VideoMode width height _ _ _ _) = vm
        winM <- GLFW.createWindow width height "Test Window" monitor Nothing
        case winM of
            Nothing  -> return ()
            Just win -> do
                GLFW.makeContextCurrent winM
                GLFW.swapInterval 1
                GLFW.setCursorInputMode win GLFW.CursorInputMode'Disabled
                
                GL.clearColor $= GL.Color4 0.2 0.2 0.5 1

                let vertices = [-1,-1
                               ,1,-1
                               ,-1,1
                               ,1,1::Float]
                    vertexAttribute = GL.AttribLocation 0
                    cameraPos = V3 0 0 (3 :: Float)
                    cameraDir = V3 1 0 (0 :: Float)
                    cameraUp  = V3 0 0 (1 :: Float)

                vbo <- makeBuffer GL.ArrayBuffer vertices
                
                vao <- GL.genObjectName
                GL.bindVertexArrayObject $= Just vao
                GL.bindBuffer GL.ArrayBuffer $= Just vbo

                GL.vertexAttribPointer vertexAttribute $=
                    (GL.ToFloat, GL.VertexArrayDescriptor 2 GL.Float 0 nullPtr)

                GL.vertexAttribArray vertexAttribute $= GL.Enabled

                vs <- loadShaderBS "simple.vert" GL.VertexShader $(embedFile "app/simple.vert") -- Shaders are part of the executable
                fs <- loadShaderBS "camera.frag" GL.FragmentShader $(embedFile "app/camera.frag")

                program <- linkShaderProgram [vs, fs]
                GL.attribLocation program "vertex_pos" $= vertexAttribute
                posLoc <- GL.uniformLocation program "camera_pos"
                dirLoc <- GL.uniformLocation program "camera_dir"
                upLoc  <- GL.uniformLocation program "camera_up"
                ratLoc <- GL.uniformLocation program "rat"

                

                GL.currentProgram $= Just program

                asUniform cameraPos posLoc
                asUniform cameraDir dirLoc
                asUniform cameraUp  upLoc
                GL.uniform ratLoc $= (fromIntegral width/fromIntegral height :: Float) -- Aspect ratio

                {-GLFW.setWindowSizeCallback win $ Just (\w x y -> do
                    GL.viewport $= (GL.Position 0 0, GL.Size (fromIntegral x) (fromIntegral y))
                    GL.uniform ratLoc $= (fromIntegral x / fromIntegral y :: Float))-} -- Commented out because we're using fullscreen

                vs' <- loadShaderBS "passthrough.vert" GL.VertexShader $(embedFile "app/passthrough.vert") -- Invalid operation here
                fs' <- loadShaderBS "shade.frag" GL.FragmentShader $(embedFile "app/shade.frag")
                cs  <- loadShaderBS "sppm.glsl" GL.ComputeShader $(embedFile "app/sppm.glsl")
                program' <- linkShaderProgram [vs', fs']
                programC <- linkShaderProgram [cs]
                posLoc' <- GL.uniformLocation programC "camera_pos"
                {-phoLoc  <- GL.uniformLocation programC "photon_map"
                hitLoc   <- GL.uniformLocation programC "hit_map"
                resLoc  <- GL.uniformLocation program' "photon_map"-}
                GL.attribLocation program' "vertex_pos" $= vertexAttribute   

                GL.currentProgram $= Just programC        
                asUniform cameraPos posLoc'

                framebuffer <- GL.genObjectName :: IO GL.FramebufferObject
                GL.bindFramebuffer GL.Framebuffer $= framebuffer
                GL.activeTexture $= GL.TextureUnit 0
                fbTexture <- GL.genObjectName
                GL.textureBinding GL.Texture2D $= Just fbTexture
                GL.texImage2D GL.Texture2D GL.NoProxy 0 GL.RGBA32F (GL.TextureSize2D (fromIntegral width) (fromIntegral height)) 0 (GL.PixelData GL.Red GL.Float nullPtr)
                GL.textureFilter GL.Texture2D $= ((GL.Nearest, Just GL.Nearest), GL.Nearest)
                GL.framebufferTexture2D GL.Framebuffer (GL.ColorAttachment 0) GL.Texture2D fbTexture 0
                depthbuffer <- GL.genObjectName :: IO GL.RenderbufferObject
                GL.bindRenderbuffer GL.Renderbuffer $= depthbuffer
                GL.renderbufferStorage GL.Renderbuffer GL.DepthComponent' (GL.RenderbufferSize (fromIntegral width) (fromIntegral height))
                GL.framebufferRenderbuffer GL.Framebuffer GL.DepthAttachment GL.Renderbuffer depthbuffer
                --GL.uniform hitLoc $= GL.TextureUnit 0

                GL.activeTexture $= GL.TextureUnit 1
                photons <- GL.genObjectName
                GL.textureBinding GL.Texture2D $= Just photons
                GL.texImage2D GL.Texture2D GL.NoProxy 0 GL.R32UI (GL.TextureSize2D (fromIntegral width) (fromIntegral height)) 0 (GL.PixelData GL.RedInteger GL.UnsignedInt nullPtr)
                let (GL.TextureObject photons_raw) = photons

                GLR.glBindImageTexture 0 photons_raw 0 0 0 GLR.GL_READ_WRITE GLR.GL_R32UI
                
                printErrorMsg "OpenGL error: " -- Check for OpenGL errors
                
                run win program program' programC framebuffer photons posLoc' cameraPos cameraDir cameraUp posLoc dirLoc upLoc clockSession_ $ network win

cursorPos :: GLFW.Window -> Wire s () IO a (Double, Double)
cursorPos win = mkGen_ $ \_ -> do
    pos <- GLFW.getCursorPos win
    GLFW.setCursorPos win 0 0
    return $ Right pos

network :: HasTime t s => GLFW.Window -> Wire s () IO a (Double, Double)
network = cursorPos

mouseSensitivity = 0.01

adjustDirUp :: V3 Float -> V3 Float -> (Float, Float) -> (V3 Float, V3 Float)
adjustDirUp dir up (x, y)
    | view _z dir > 0.95  = (dir' - V3 0 0 0.01, normalize $ cross right (dir' - V3 0 0 0.01))
    | view _z dir < -0.95 = (dir' + V3 0 0 0.01, normalize $ cross right (dir' + V3 0 0 0.01))
    | otherwise           = (dir', normalize $ cross right dir')
    where dir'  = normalize $ dir - (up * pure (mouseSensitivity * y)) - (right * pure (mouseSensitivity * x))
          right = normalize $ cross dir $ V3 0 0 1--cross dir up

render cProgram fProgram sProgram framebuffer photons = do
    GL.bindFramebuffer GL.Framebuffer $= framebuffer
    GL.drawBuffer $= GL.FBOColorAttachment 0
    GL.clear [GL.ColorBuffer]
    GL.currentProgram $= Just cProgram
    GL.drawArrays GL.TriangleStrip 0 4

    GL.bindFramebuffer GL.Framebuffer $= GL.defaultFramebufferObject

    let (GL.TextureObject photons_raw) = photons
    GLR.glClearTexImage photons_raw 0 GLR.GL_RED_INTEGER GLR.GL_UNSIGNED_INT nullPtr
    GL.currentProgram $= Just sProgram
    GLR.glDispatchCompute 32 32 1

    GL.currentProgram $= Just fProgram
    GL.drawBuffer $= GL.BackBuffers
    GL.drawArrays GL.TriangleStrip 0 4
    --printError

adjustPos win dir up pos = do
    forward <- GLFW.getKey win GLFW.Key'Comma
    back    <- GLFW.getKey win GLFW.Key'O
    left    <- GLFW.getKey win GLFW.Key'A
    right   <- GLFW.getKey win GLFW.Key'E
    space   <- GLFW.getKey win GLFW.Key'Space
    shift   <- GLFW.getKey win GLFW.Key'LeftShift
    let f = case forward of
            GLFW.KeyState'Pressed -> 0.2 :: Float
            _                -> 0
        b = case back of
            GLFW.KeyState'Pressed -> 0.1 :: Float
            _                -> 0
        l = case left of
            GLFW.KeyState'Pressed -> 0.1 :: Float
            _                -> 0
        r = case right of
            GLFW.KeyState'Pressed -> 0.1 :: Float
            _                -> 0
        d = case shift of
            GLFW.KeyState'Pressed -> 0.1 :: Float
            _                -> 0
        u = case space of
            GLFW.KeyState'Pressed -> 0.1 :: Float
            _                -> 0
        dir2 = normalize $ dir * V3 1 1 0
        left2 = normalize $ cross dir up * V3 1 1 0
    return $ (dir2 * pure (f - b)) + (left2 * pure (l - r)) + pos + V3 0 0 (u - d)

run :: (HasTime t s) => GLFW.Window -> GL.Program -> GL.Program -> GL.Program -> GL.FramebufferObject -> GL.TextureObject -> GL.UniformLocation -> V3 Float -> V3 Float -> V3 Float -> GL.UniformLocation -> GL.UniformLocation -> GL.UniformLocation -> Session IO s -> Wire s e IO a (Double, Double) -> IO ()
run win cProgram fProgram sProgram framebuffer photons posLoc' cameraPos cameraDir cameraUp posLoc dirLoc upLoc session wire = do
    GLFW.pollEvents        
    shouldclose <- GLFW.windowShouldClose win
    unless shouldclose $ do
        (st , session') <- stepSession session
        (wt', wire'   ) <- stepWire wire st $ Right undefined

        case wt' of
            Left  _ -> return ()
            Right (x,y) -> do
                let (dir, up) = adjustDirUp cameraDir cameraUp (double2Float x, double2Float y)
                pos <- adjustPos win dir up cameraPos

                GL.currentProgram $= Just cProgram
                asUniform dir dirLoc
                asUniform up upLoc
                asUniform pos posLoc
                GL.currentProgram $= Just sProgram
                asUniform pos posLoc'
                GL.clear [GL.ColorBuffer]

                render cProgram fProgram sProgram framebuffer photons

                GLFW.swapBuffers win
        
                run win cProgram fProgram sProgram framebuffer photons posLoc' pos dir up posLoc dirLoc upLoc session' wire'